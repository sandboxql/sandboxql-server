# sandboxql/sandboxql-server
The server-side GraphQL API for sandboxql.

## Setup
Create your own secrets.yml file from the example. Generate appropriate JSON Web Token access and refresh keys. Fill in the mail credentials.

## Install the dependencies
```bash
yarn install
```

## Run service offline
```bash
docker-compose up
```

## Re-build the image
```bash
docker-compose up --build
```

## Notes
You can access the shells for our two images with the following (example) commands:

```bash
docker exec -it sandboxql-server_serverless_1 /bin/ash
```

```bash
docker exec -it sandboxql-server_mongo_1 /bin/ash
```

`/bin/ash` is Ash ([Almquist Shell](https://www.in-ulm.de/~mascheck/various/ash/#busybox)) provided by BusyBox

## Todo
- Add updateParentId/path method to group
- Encrypted env variables
- Websockets
- Container need async/await
- Password buffer has been deprecated in Node 10