import { GraphQLScalarType } from 'graphql';
import { makeExecutableSchema } from 'graphql-tools';
import { Kind,  } from 'graphql/language';

const typeDefs = `
  scalar Date
  
  type Query {
    testDate(ISOString: String!): Date
  }
`;

const resolvers = {
  Query: {
    testDate: (_: any, { ISOString }: { ISOString: string }) => new Date(ISOString)
  },
  
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date/time custom scalar type.',
    parseValue: (value: string) => new Date(value), // value from the client
    serialize: (value: Date) => value.toISOString(), // value sent to the client
    parseLiteral: (ast: any) => {
      if (ast.kind === Kind.STRING) {
        return new Date(ast.value) // ast value is always in string format
      }
      return null;
    }
  })
}

export default makeExecutableSchema({
  typeDefs,
  resolvers
});