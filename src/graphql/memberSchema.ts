import { IResolvers, makeExecutableSchema } from 'graphql-tools';
import MemberService from '../domain/Member/MemberService';
import { response } from '../response';

const typeDefs = `
  scalar Date
  scalar UUID

  type Member {
    id: UUID!
    userId: String!
    scope: String!
    kind: String!
    itemId: UUID!
    created: Date!
  }

  type Mutation {
    required: Boolean
  }

  type Query {
    membersByItemId(itemId: UUID!): [Member]
  }
`;

const resolvers: IResolvers = {
  Query: {
    membersByItemId: async (
      _: any, { itemId }: { itemId: string }, { context }: any
    ): Promise<any> => {
      let memberService: MemberService = context.container.get('memberService');
      let payload = await memberService.fetchAllByItemId(itemId);
      return response(payload);
    }
  },

  Mutation: {}
}

export default makeExecutableSchema({ typeDefs, resolvers });