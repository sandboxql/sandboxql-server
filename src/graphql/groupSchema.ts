import { IResolvers, makeExecutableSchema } from 'graphql-tools';
import { checkAuth } from '../checkAuth';
import { GroupVisibility } from '../domain/Group/GroupEntity';
import GroupService from '../domain/Group/GroupService';
import { response } from '../response';

const typeDefs = `
  scalar Date
  scalar UUID

  enum GroupVisibility {
    private,
    public
  }

  input CreateGroupInput {
    parentId: UUID
    name: String!
    path: String
    description: String
    visibility: GroupVisibility
  }

  input UpdateGroupInput {
    id: UUID!
    name: String!
    path: String
    description: String
    visibility: GroupVisibility!
  }

  type Group {
    id: UUID!
    parentId: UUID
    name: String!
    path: String
    description: String
    visibility: GroupVisibility!
    schema: String!
    created: Date!
    updated: Date
  }

  type Mutation {
    createGroup(input: CreateGroupInput!): Group
    updateGroup(input: UpdateGroupInput!): Group
  }

  type Query {
    groupById(id: UUID!): Group
    groupsByUser: [Group]
  }
`;

const resolvers: IResolvers = {
  Query: {
    groupById: async (
      _: any, { id }: { id: string }, { context, headers }: any
    ): Promise<any> => {
      let user = await checkAuth(context, headers, false);
      let groupService: GroupService = context.container.get('groupService');
      let payload = await groupService.fetch(user.getOutput(), id);

      return response(payload);
    },

    groupsByUser: async (
      _: any, {}, { context, headers }: any
    ): Promise<any> => {
      let user = await checkAuth(context, headers);
      let groupService: GroupService = context.container.get('groupService');
      let payload = await groupService.browse(user.getOutput());

      return response(payload);
    }
  },

  Mutation: {
    createGroup: async (
      _: any, { input }: { input: {
        parentId: string,
        name: string,
        path: string,
        visibility: GroupVisibility
        description: string,
      } }, { context, headers }: any
    ): Promise<any> => {
      let user = await checkAuth(context, headers);
      let groupService: GroupService = context.container.get('groupService');
      let payload = await groupService.create(
        user.getOutput(),
        input.name,
        input.path || null,
        input.visibility || 'public',
        input.description || null,
        input.parentId || null
      );

      return response(payload);
    },

    updateGroup: async (
      _: any, { input }: { input: {
        id: string,
        name: string,
        path: string | undefined
        description: string | undefined
        visibility: GroupVisibility,
      } }, { context, headers }: any
    ): Promise<any> => {
      let user = await checkAuth(context, headers);
      let groupService: GroupService = context.container.get('groupService');
      let payload = await groupService.update(
        user.getOutput(),
        input.id,
        input.name,
        input.path || null,
        input.visibility,
        input.description || null
      );

      return response(payload);
    }
  }
}

export default makeExecutableSchema({ typeDefs, resolvers });