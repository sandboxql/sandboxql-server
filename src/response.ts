import { PayloadInterface, PayloadStatus } from '@dupkey/payload';
import { ApolloError, UserInputError, AuthenticationError } from 'apollo-server-core';

export const response = (payload: PayloadInterface) => {
  switch (payload.getStatus()) {
    case 'ACCEPTED': PayloadStatus.ACCEPTED
      return payload.getOutput();
    
    case 'CREATED': PayloadStatus.CREATED
      return payload.getOutput();

    case 'FOUND': PayloadStatus.FOUND
      return payload.getOutput();

    case 'NOT_AUTHENTICATED': PayloadStatus.NOT_AUTHENTICATED
      return new AuthenticationError(
        'Authentication required'
      );
    
    case 'NOT_AUTHORIZED': PayloadStatus.NOT_AUTHORIZED
      return new ApolloError(
        'Unauthorized',
        '401'          
      );
    
    case 'NOT_CREATED': PayloadStatus.NOT_CREATED
      return null;

    case 'NOT_FOUND': PayloadStatus.NOT_FOUND
      return null;
    
    case 'NOT_UPDATED': PayloadStatus.NOT_UPDATED
      return null;

    case 'NOT_VALID': PayloadStatus.NOT_VALID
      return new UserInputError('Invalid input', {
        invalidArgs: payload.getErrors()
      });

    case 'PROCESSING': PayloadStatus.PROCESSING
      return payload.getInput();
    
    case 'UPDATED': PayloadStatus.UPDATED
      return payload.getOutput();

    default:
      console.error('The payload status that the domain responded with does not exist in the response. Did you forget to add it?');
      return new ApolloError(
        'The payload status that the domain responded with does not exist in the response. Did you forget to add it?',
        '500'          
      );
  }
}