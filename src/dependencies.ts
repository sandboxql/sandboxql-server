import { ContainerBuilderInterface } from '@dupkey/container';
import Jwt from '@dupkey/jwt';
import { Email, Mailer, MailgunTransport } from '@dupkey/mail';
import * as Mailgun from 'mailgun-js';
import AuthTokenRepository from './domain/Account/AuthToken/AuthTokenRepository';
import AuthTokenService from './domain/Account/AuthToken/AuthTokenService';
import AuthTokenValidator from './domain/Account/AuthToken/AuthTokenValidator';
import UserMessage from './domain/Account/User/UserMessage';
import UserRepository from './domain/Account/User/UserRepository';
import UserService from './domain/Account/User/UserService';
import UserValidator from './domain/Account/User/UserValidator';
import GroupMessage from './domain/Group/GroupMessage';
import GroupRepository from './domain/Group/GroupRepository';
import GroupService from './domain/Group/GroupService';
import GroupValidator from './domain/Group/GroupValidator';
import MemberRepository from './domain/Member/MemberRepository';
import MemberService from './domain/Member/MemberService';
import { message } from './message';

const jwt: Map<string, ContainerBuilderInterface> = new Map([
  [
    'jsonWebToken', {
      definition: Jwt,
      dependencies: [
        process.env.JWT_ACCESS_SECRET,
        process.env.JWT_REFRESH_SECRET
      ]
    }
  ]
]);

const mail: Map<string, ContainerBuilderInterface> = new Map([
  [
    'mailgun', {
      definition: Mailgun,
      dependencies: [{
        apiKey: process.env.MAILGUN_KEY,
        domain: process.env.MAILGUN_DOMAIN
      }]
    }
  ],[
    'transport', {
      definition: MailgunTransport,
      dependencies: ['mailgun']
    }
  ],[
    'mailer', {
      definition: Mailer,
      dependencies: [
        'transport',
        new Email('support'.concat('@', String(process.env.SENDMAIL_HOST)), process.env.CLIENT_NAME),
        { client: process.env.CLIENT_NAME, uri: process.env.CLIENT_URI },
        (process.env.SENDMAIL === 'true')
      ]
    }
  ],[
    'message', {
      definition: message
    }
  ],[
    'groupMessage', {
      definition: GroupMessage,
      dependencies: ['mailer', message.group]
    }
  ],[
    'userMessage', {
      definition: UserMessage,
      dependencies: ['mailer', message.user]
    }
  ]
]);

const repository: Map<string, ContainerBuilderInterface> = new Map([
  [
    'authTokenRepository', {
      definition: AuthTokenRepository,
      dependencies: ['connection']
    }
  ],[
    'groupRepository', {
      definition: GroupRepository,
      dependencies: ['connection']
    }
  ],[
    'memberRepository', {
      definition: MemberRepository,
      dependencies: ['connection']
    }
  ],[
    'userRepository', {
      definition: UserRepository,
      dependencies: ['connection']
    }
  ]
]);

const validator: Map<string, ContainerBuilderInterface> = new Map([
  [
    'authTokenValidator', {
      definition: AuthTokenValidator
    }
  ],[
    'groupValidator', {
      definition: GroupValidator
    }
  ],[
    'userValidator', {
      definition: UserValidator
    }
  ]
]);

const service: Map<string, ContainerBuilderInterface> = new Map([
  [
    'authTokenService', {
      definition: AuthTokenService,
      dependencies: [
        'authTokenRepository',
        'authTokenValidator',
        'jsonWebToken',
        'userRepository'
      ]
    }
  ],[
    'groupService', {
      definition: GroupService,
      dependencies: [
        'memberRepository',
        'groupMessage',
        'groupRepository',
        'groupValidator'
      ]
    }
  ],[
    'memberService', {
      definition: MemberService,
      dependencies: [
        'memberRepository'
      ]
    }
  ],[
    'userService', {
      definition: UserService,
      dependencies: [
        'userMessage',
        'userRepository',
        'userValidator'
      ]
    }
  ]
]);

const dependencies: Map<string, ContainerBuilderInterface> = new Map([
  ...jwt,
  ...mail,
  ...repository,
  ...validator,
  ...service
]);

export default dependencies;