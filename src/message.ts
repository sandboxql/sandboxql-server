import { MessageSettings } from '@dupkey/mail';

interface Message {
  [key: string]: MessageSettings;
}

export const message: Message = {
  group: {
    forCreate: {
      template: 'groupmessage.forcreate',
      subject: 'Access to %recipient.groupName% %recipient.groupSchema% was granted'
    }
  },
  user: {
    forCreate: {
      template: 'usermessage.forcreate',
      subject: 'User Registration'
    },
    forExists: {
      template: 'usermessage.forexists',
      subject: 'Account Access'
    }
  }
}