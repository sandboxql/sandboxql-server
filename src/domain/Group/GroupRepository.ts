import Uuid from '@dupkey/uuid';
import * as mariadb from 'mariadb';
import { GroupEntity } from './GroupEntity';

export default class GroupRepository
{
  private database: mariadb.Connection;

  constructor(database: mariadb.Connection)
  {
    this.database = database;
  }

  private createInstance(group: any): GroupEntity
  {
    return new GroupEntity(
      Uuid.fromBuffer(group.id),
      group.name,
      group.schema,
      group.visibility,
      group.created,
      group.path,      
      group.description,
      (group.parentId === null) ? null : Uuid.fromBuffer(group.parentId),
      group.updated,
      group.scope
    );
  }

  public async fetchAllByUserId(userId: Uuid): Promise<GroupEntity[]>
  {
    let sql = " \
      WITH RECURSIVE `cte` (`id`, `parentId`, `name`, `path`, `description`, `schema`, `visibility`, `created`, `updated`) AS ( \
        SELECT `group`.* \
        FROM `group`, `member` \
        WHERE `member`.`userId` = :userId \
          AND `member`.`kind` = 'group' \
          AND `member`.`itemId` = `group`.`id` \
        UNION DISTINCT \
          SELECT `p`.* \
          FROM `group` AS `p` \
          INNER JOIN `cte` \
            ON `p`.`parentId` = `cte`.`id` \
      ) \
      SELECT * \
      FROM `cte` \
    ";

    let results = await this.database.query(sql, { userId: userId.getBuffer() });

    return results.map((result: any) => this.createInstance(result));
  }

  public async fetchOneById(id: Uuid): Promise<GroupEntity | null> {
    let sql = " \
      SELECT * \
      FROM `group` \
      WHERE `group`.`id` = :id \
    ";

    let result = await this.database.query(sql, { id: id.getBuffer() });
    if (result.length === 1) {
      return this.createInstance(result[0]);
    }

    return null;
  }

  public async fetchOneByPathParentId(path: string | null, parentId: Uuid | null): Promise<GroupEntity | null>
  {
    let where1 = "WHERE `group`.`path` = :path";
    if (path === null) {
      where1 = "WHERE `group`.`path` IS NULL";
    }

    let parent: Buffer | null = null;
    let where2 = " AND `group`.`parentId` = :parentId"
    if (parentId === null) {
      where2 = " AND `group`.`parentId` IS NULL";
    } else {
      parent = parentId.getBuffer();
    }

    let sql = 'SELECT * FROM `group` ' + where1 + where2;

    let result = await this.database.query(sql, { parentId: parent, path });
    if (result.length === 1) {
      return this.createInstance(result[0]);
    }

    return null;
  }

  public async save(group: GroupEntity): Promise<any>
  {
    let sql = " \
      INSERT INTO `group` \
      (`id`, `parentId`, `name`, `path`, `description`, `schema`, `visibility`, `created`) VALUES \
      (:id, :parentId, :name, :path, :description, :schema, :visibility, :created) \
    ";

    let values = {
      id: group.getId().getBuffer(),
      parentId: (group.getParentId() === null) ? null : group.getParentId()!.getBuffer(),
      name: group.getName(),
      path: group.getPath(),
      description: group.getDescription(),
      schema: group.getSchema(),
      visibility: group.getVisibility(),
      created: group.getCreated()
    }

    return await this.database.query(sql, values);
  }

  public async update(group: GroupEntity): Promise<any>
  {
    let sql = " \
      UPDATE `group` \
      SET `group`.`name` = :name, \
        `group`.`path` = :path, \
        `group`.`description` = :description, \
        `group`.`visibility` = :visibility, \
        `group`.`updated` = :updated \
      WHERE `group`.`id` = :id \
    ";

    let values = {
      id: group.getId().getBuffer(),
      name: group.getName(),
      path: group.getPath(),
      description: group.getDescription(),
      visibility: group.getVisibility(),
      updated: group.getUpdated()
    }

    return await this.database.query(sql, values);
  }
}
