import { Payload, PayloadInterface, PayloadStatus } from '@dupkey/payload';
import Uuid from '@dupkey/uuid';
import UserEntity from '../Account/User/UserEntity';
import { MemberEntity } from '../Member/MemberEntity';
import MemberRepository from '../Member/MemberRepository';
import { GroupEntity, GroupVisibility } from './GroupEntity';
import GroupMessage from './GroupMessage';
import GroupRepository from './GroupRepository';
import GroupValidator from './GroupValidator';

export default class GroupService
{
  private memberRepository: MemberRepository;
  private groupMessage: GroupMessage;
  private groupRepository: GroupRepository;
  private groupValidator: GroupValidator;

  constructor(
    memberRepository: MemberRepository,
    groupMessage: GroupMessage,
    groupRepository: GroupRepository,
    groupValidator: GroupValidator
  ) {
    this.memberRepository = memberRepository;
    this.groupMessage = groupMessage;
    this.groupRepository = groupRepository;
    this.groupValidator = groupValidator;
  }

  public async browse(user: UserEntity): Promise<PayloadInterface>
  {
    let payload = (new Payload()).setInput({ user });

    let groups = await this.groupRepository.fetchAllByUserId(user.getId());
    if (groups.length === 0) {
      return payload.setStatus(PayloadStatus.NOT_FOUND);
    }

    return payload.setOutput(groups).setStatus(PayloadStatus.FOUND);
  }

  public async create(
    user: UserEntity,
    name: string,
    path: string | null = null,
    visibility: GroupVisibility = 'public',
    description: string | null = null,
    parentId: string | null = null,
    schema: string = 'group',
  ): Promise<PayloadInterface>
  {
    let payload = (new Payload()).setInput({ user, name, path, visibility, description, parentId, schema });

    if (this.groupValidator.forCreate(name, path, visibility, description, parentId, schema) === false) {
      return payload.setStatus(PayloadStatus.NOT_VALID)
        .setErrors(this.groupValidator.getErrors());
    }

    let parent: Uuid | null = null;
    if (parentId !== null) {
      parent = Uuid.fromString(parentId);
      let members = await this.memberRepository.fetchAllByGroupIdUserId(parent, user.getId());
      let scope = members.some((member: MemberEntity) => member.getScope() === 'admin');
      
      if (scope === false) {
        return payload.setStatus(PayloadStatus.NOT_AUTHORIZED);
      }
    }

    let group;
    if (path !== null) {
      group = await this.groupRepository.fetchOneByPathParentId(path, parent);
      if (group !== null) {
        return payload.setStatus(PayloadStatus.NOT_VALID)
          .setErrors([{ title: 'Path has already been taken', source: ['Group.create.path'] }]);
      }
    }

    group = new GroupEntity(
      Uuid.v4(),
      name,
      schema,
      visibility,
      new Date(),
      path,
      description,
      parent
    );

    await this.groupRepository.save(group);

    if (parentId === null) {
      let member = new MemberEntity(
        Uuid.v4(),
        user.getId(),
        'admin',
        'group',
        group.getId(),
        new Date()
      );

      await this.memberRepository.save(member);
    }
    
    await this.groupMessage.forCreate(group, user);

    return payload.setStatus(PayloadStatus.CREATED).setOutput(group);
  }

  public async fetch(user: UserEntity | null, id: string): Promise<PayloadInterface>
  {
    let payload = (new Payload()).setInput({ user, id });

    if (this.groupValidator.forFetch(id) === false) {
      return payload.setStatus(PayloadStatus.NOT_VALID)
        .setErrors(this.groupValidator.getErrors());
    }

    let group = await this.groupRepository.fetchOneById(Uuid.fromString(id));
    if (group === null) {
      return payload.setStatus(PayloadStatus.NOT_FOUND);
    }

    if (group.getVisibility() === 'public') {
      return payload.setStatus(PayloadStatus.FOUND).setOutput(group);
    }

    if (user === null) {
      return payload.setStatus(PayloadStatus.NOT_AUTHENTICATED);
    }

    let members = await this.memberRepository.fetchAllByGroupIdUserId(group.getId(), user.getId());
    let scope = members.some((member: MemberEntity) =>
      member.getScope() === 'admin' ||
      member.getScope() === 'moderator' ||
      member.getScope() === 'member'
    );
    
    if (scope === false) {
      return payload.setStatus(PayloadStatus.NOT_AUTHORIZED);
    }

    return payload.setStatus(PayloadStatus.FOUND).setOutput(group);
  }

  // public async fetchByPath(user: UserEntity | null, path: string, parentId: string | null = null): Promise<PayloadInterface>
  // {
  //   let payload = (new Payload()).setInput({ user, path, parentId });

  //   if (this.groupValidator.forFetchByPath(path) === false) {
  //     return payload.setStatus(PayloadStatus.NOT_VALID)
  //       .setErrors(this.groupValidator.getErrors());
  //   }

  //   let parent: Uuid | null = null;
  //   if (parentId !== null) {
  //     parent = Uuid.fromString(parentId);
  //   }

  //   let group = await this.groupRepository.fetchOneByPathParentId(path, parent);
    
  //   if (group === null) {
  //     return payload.setStatus(PayloadStatus.NOT_FOUND);
  //   }

  //   if (group.getVisibility() === 'public') {
  //     return payload.setStatus(PayloadStatus.FOUND).setOutput(group);
  //   }

  //   if (user === null) {
  //     return payload.setStatus(PayloadStatus.NOT_AUTHENTICATED);
  //   }

  //   // recursive permissions check
  //   let member = await this.memberRepository.fetchOneByUserIdItemId(user.getId(), group.getId());
  //   if (member === null) {
  //     return payload.setStatus(PayloadStatus.NOT_AUTHORIZED);
  //   }

  //   return payload.setStatus(PayloadStatus.FOUND).setOutput(group);
  // }

  public async update(
    user: UserEntity,
    id: string,
    name: string,
    path: string | null,
    visibility: GroupVisibility,
    description: string | null
  ): Promise<PayloadInterface>
  {
    let payload = (new Payload()).setInput({ user, id, name, path, visibility, description });

    if (this.groupValidator.forUpdate(id, name, path, visibility, description) === false) {
      return payload.setStatus(PayloadStatus.NOT_VALID)
        .setErrors(this.groupValidator.getErrors());
    }

    let group = await this.groupRepository.fetchOneById(Uuid.fromString(id));
    if (group === null) {
      return payload.setStatus(PayloadStatus.NOT_FOUND);
    }

    let members = await this.memberRepository.fetchAllByGroupIdUserId(group.getId(), user.getId());
    let scope = members.some((member: MemberEntity) => member.getScope() === 'admin');
    
    if (scope === false) {
      return payload.setStatus(PayloadStatus.NOT_AUTHORIZED);
    }

    if (path !== null) {
      let pathCheck = await this.groupRepository.fetchOneByPathParentId(path, group.getParentId());
      if (pathCheck !== null && pathCheck.getId().toString() !== group.getId().toString()) {
        return payload.setStatus(PayloadStatus.NOT_VALID)
          .setErrors([{ title: 'Path has already been taken', source: ['Group.update.path'] }]);
      }
    }

    group.setName(name).setPath(path).setDescription(description).setVisibility(visibility).setUpdated(new Date());
    this.groupRepository.update(group);

    return payload.setStatus(PayloadStatus.FOUND).setOutput(group);
  }
}