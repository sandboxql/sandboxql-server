import Uuid from '@dupkey/uuid';

export type GroupVisibility = 'private' | 'public';

export class GroupEntity
{
  private id: Uuid;
  private name: string;
  private schema: string;
  private visibility: GroupVisibility;
  private created: Date;
  private path: string | null;
  private description: string | null;
  private parentId: Uuid | null;
  private updated: Date | null;
  private scope: string | null;

  constructor(
    id: Uuid,
    name: string,
    schema: string,
    visibility: GroupVisibility,
    created: Date,
    path: string | null = null,
    description: string | null = null,
    parentId: Uuid | null = null,
    updated: Date | null = null,
    scope: string | null = null
  ) {
    this.id = id;
    this.name = name;
    this.schema = schema;
    this.visibility = visibility;
    this.created = created;
    this.path = path;
    this.description = description;
    this.parentId = parentId;
    this.updated = updated;
    this.scope = scope;
  }

  public getId(): Uuid
  {
    return this.id;
  }

  public getName(): string
  {
    return this.name;
  }

  public setName(name: string): GroupEntity
  {
    this.name = name;
    return this;
  }

  public getSchema(): string {
    return this.schema;
  }

  public getVisibility(): GroupVisibility
  {
    return this.visibility;
  }

  public setVisibility(visibility: GroupVisibility): GroupEntity
  {
    this.visibility = visibility;
    return this;
  }

  public getCreated(): Date
  {
    return this.created;
  }

  public setCreated(created: Date): GroupEntity
  {
    this.created = created;
    return this;
  }

  public getPath(): string | null {
    return this.path;
  }

  public setPath(path: string | null): GroupEntity
  {
    this.path = path;
    return this;
  }

  public getDescription(): string | null
  {
    return this.description;
  }

  public setDescription(description: string | null): GroupEntity
  {
    this.description = description;
    return this;
  }

  public getParentId(): Uuid | null
  {
    return this.parentId;
  }

  public setParentId(parentId: Uuid): GroupEntity
  {
    this.parentId = parentId;
    return this;
  }

  public getUpdated(): Date | null
  {
    return this.updated;
  }

  public setUpdated(updated: Date): GroupEntity
  {
    this.updated = updated;
    return this;
  }

  public getScope(): string | null
  {
    return this.scope;
  }
}
