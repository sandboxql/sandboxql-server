import { JoiValidator } from '@dupkey/validator';
import * as Joi from 'joi';

export default class GroupValidator extends JoiValidator
{
  public forCreate(name: string, path: string | null, visibility: string, description: string | null, parentId: string | null, schema: string): boolean
  {
    let input = {
      name,
      path,
      visibility,
      description,
      parentId,
      schema
    }

    let rules = {
      name: Joi.string().min(3).max(45).required(),
      path: Joi.string().min(3).max(55).regex(/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/, { name: 'uri' }).allow(null),
      visibility: Joi.string().valid('public', 'private').required(),
      description: Joi.string().max(191).allow(null),
      parentId: Joi.string().guid({ version: ['uuidv4']}).allow(null),
      schema: Joi.string().min(3).max(45).required()
    };

    return this.validate(input, rules);
  }

  public forFetch(id: string)
  {
    let input = { id }

    let rules = {
      id: Joi.string().guid({ version: [ 'uuidv4' ] }).required()
    }

    return this.validate(input, rules);
  }

  public forFetchByPath(path: string)
  {
    let input = { path }

    let rules = {
      path: Joi.string().regex(/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/, { name: 'uri' }),
    }

    return this.validate(input, rules);
  }

  public forUpdate(id: string, name: string, path: string | null, visibility: string, description: string | null): boolean
  {
    let input = {
      id,
      name,
      path,
      visibility,
      description
    }

    let rules = {
      id: Joi.string().guid({ version: ['uuidv4']}).required(),
      name: Joi.string().min(3).max(45).required(),
      path: Joi.string().min(3).max(55).regex(/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/, { name: 'uri' }).allow(null),
      visibility: Joi.string().valid('public', 'private').required(),
      description: Joi.string().max(191).allow(null)
    };

    return this.validate(input, rules);
  }
}