import { Mailer, Message, MessageSettings, ResponseInterface } from '@dupkey/mail';
import UserEntity from '../Account/User/UserEntity';
import { GroupEntity } from './GroupEntity';

export default class UserMessage
{
  protected mailer: Mailer;
  protected message: MessageSettings;

  constructor(mailer: Mailer, message: MessageSettings)
  {
    this.mailer = mailer;
    this.message = message;
  }

  public async forCreate(group: GroupEntity, user: UserEntity): Promise<ResponseInterface>
  {
    let message = new Message(
      user.getEmail(),
      this.message.forCreate.template,
      this.message.forCreate.subject,
      {
        groupId: group.getId(),
        groupName: group.getName(),
        groupSchema: group.getSchema(),
        scope: 'admin'
      }
    );

    return await this.mailer.send(message);
  }
}