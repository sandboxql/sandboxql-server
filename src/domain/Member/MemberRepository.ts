import Uuid from '@dupkey/uuid';
import * as mariadb from 'mariadb';
import { MemberEntity } from './MemberEntity';

export default class MemberRepository
{
  private database: mariadb.Connection;

  constructor(database: mariadb.Connection)
  {
    this.database = database;
  }

  private createInstance(member: any): MemberEntity
  {
    return new MemberEntity(
      Uuid.fromBuffer(member.id),
      Uuid.fromBuffer(member.userId),
      member.scope,
      member.kind,
      Uuid.fromBuffer(member.itemId),
      member.created
    );
  }

  public async fetchAllByItemId(itemId: Uuid): Promise<MemberEntity[]>
  {
    let sql = " \
      SELECT * \
      FROM `member` \
      WHERE `member`.`itemId` = :itemId \
    ";

    let results = await this.database.query(sql, { itemId: itemId.getBuffer() });

    return results.map((result: any) => this.createInstance(result));
  }

  // public async fetchOneByUserIdItemId(userId: Uuid, itemId: Uuid, kind: string = 'group'): Promise<MemberEntity | null>
  // {
  //   let sql = " \
  //     SELECT * \
  //     FROM `member` \
  //     WHERE `member`.`userId` = :userId \
  //       AND `member`.`kind` = :kind \
  //       AND `member`.`itemId` = :itemId \
  //   ";

  //   let result = await this.database.query(sql, {
  //     userId: userId.getBuffer(),
  //     kind,
  //     itemId: itemId.getBuffer()
  //   });

  //   if (result.length === 1) {
  //     return result.map((result: any) => this.createInstance(result));
  //   }

  //   return null;
  // }

  // Recursively traverse the tree and return groups with outer joined members by userId
  public async fetchAllByGroupIdUserId(groupId: Uuid, userId: Uuid): Promise<MemberEntity[]>
  {
    let sql = " \
      WITH RECURSIVE `cte` AS ( \
        SELECT `group`.`parentId`, `member`.* \
        FROM `group` \
        LEFT OUTER JOIN `member` \
          ON `member`.`userId` = :userId \
            AND `member`.`kind` = 'group' \
            AND `member`.`itemId` = `group`.`id` \
        WHERE `group`.`id` = :groupId \
        UNION ALL \
          SELECT `g`.`parentId`, `m`.* \
          FROM `cte` AS `c` \
          INNER JOIN `group` AS `g` \
            ON `c`.`parentId` = `g`.`id` \
          LEFT OUTER JOIN `member` AS `m` \
            ON `c`.`parentId` = `m`.`itemId` \
              AND `m`.`userId` = :userId \
      ) \
      SELECT * \
      FROM `cte` \
    ";

    let results = await this.database.query(sql, { groupId: groupId.getBuffer(), userId: userId.getBuffer() });
    results = results.filter((result: any) => result.scope !== null);

    return results.map((result: any) => this.createInstance(result));
  }

  public async save(member: MemberEntity): Promise<any>
  {
    let sql = " \
      INSERT INTO `member` \
      (id, userId, scope, kind, itemId, created) VALUES \
      (:id, :userId, :scope, :kind, :itemId, :created) \
    ";

    let values = {
      id: member.getId().getBuffer(),
      userId: member.getUserId().getBuffer(),
      scope: member.getScope(),
      kind: member.getKind(),
      itemId: member.getItemId().getBuffer(),
      created: member.getCreated()
    }

    return await this.database.query(sql, values);
  }
}
