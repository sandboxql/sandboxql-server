import { Payload, PayloadInterface, PayloadStatus } from '@dupkey/payload';
import Uuid from '@dupkey/uuid';
import MemberRepository from './MemberRepository';

export default class MemberService
{
  private memberRepository: MemberRepository;

  constructor(
    memberRepository: MemberRepository,
  ) {
    this.memberRepository = memberRepository;
  }

  public async fetchAllByItemId(itemId: string): Promise<PayloadInterface>
  {
    let payload = (new Payload()).setInput({ itemId });

    let members = await this.memberRepository.fetchAllByItemId(Uuid.fromString(itemId));
    if (members.length === 0) {
      return payload.setStatus(PayloadStatus.NOT_FOUND);
    }

    return payload.setOutput(members).setStatus(PayloadStatus.FOUND);
  }
}