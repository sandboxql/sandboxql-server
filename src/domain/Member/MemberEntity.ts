import Uuid from '@dupkey/uuid';

export type MemberScope = 'admin' | 'moderator' | 'member';
export type MemberKind = 'group' | 'app';

export class MemberEntity
{
  private id: Uuid;
  private userId: Uuid;
  private scope: MemberScope;
  private kind: MemberKind;
  private itemId: Uuid;
  private created: Date;
  private app: string | null;

  constructor(
    id: Uuid,
    userId: Uuid,
    scope: MemberScope,
    kind: MemberKind,
    itemId: Uuid,
    created: Date,
    app: string | null = null
  ) {
    this.id = id;
    this.userId = userId;
    this.scope = scope;
    this.kind = kind;
    this.itemId = itemId;
    this.created = created;
    this.app = app;
  }

  public getId(): Uuid
  {
    return this.id;
  }

  public getUserId(): Uuid
  {
    return this.userId;
  }

  public getScope(): string
  {
    return this.scope;
  }

  public setScope(scope: MemberScope): MemberEntity
  {
    this.scope = scope;
    return this;
  }

  public getKind(): MemberKind
  {
    return this.kind;
  }

  public getItemId(): Uuid
  {
    return this.itemId;
  }

  public getCreated(): Date
  {
    return this.created;
  }

  public getApp(): string | null
  {
    return this.app;
  }
}
