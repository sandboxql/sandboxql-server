import { Email } from '@dupkey/mail';
import Password from '@dupkey/password';
import Uuid from '@dupkey/uuid';
import * as mariadb from 'mariadb';
import UserEntity from './UserEntity';

export default class UserRepository
{
  private database: mariadb.Connection;

  constructor(database: mariadb.Connection)
  {
    this.database = database;
  }

  private createInstance(user: any): UserEntity
  {
    return new UserEntity(
      Uuid.fromBuffer(user.id),
      user.name,
      new Email(user.email, user.name),
      new Password(user.password),
      user.created,
      user.updated,
      user.activated,
      user.banned
    );
  }

  public async fetchOneByEmail(email: string): Promise<UserEntity | null>
  {
    let sql = " \
      SELECT * \
      FROM `account_user` \
      WHERE `account_user`.`email` = :email \
    ";

    let result = await this.database.query(sql, { email });
    if (result.length === 1) {
      return this.createInstance(result[0]);
    }

    return null;
  }

  public async fetchOneById(id: Uuid): Promise<UserEntity | null>
  {
    let sql = " \
      SELECT * \
      FROM `account_user` \
      WHERE `account_user`.`id` = :id \
    ";
    
    let result = await this.database.query(sql, { id: id.getBuffer() });
    if (result.length === 1) {
      return this.createInstance(result[0]);
    }

    return null;
  }

  public async save(user: UserEntity): Promise<object>
  {
    let sql = " \
      INSERT INTO `account_user` \
      (id, name, email, password, created) VALUES \
      (:id, :name, :email, :password, :created) \
    ";

    let values = {
      id: user.getId().getBuffer(),
      name: user.getName(),
      email: user.getEmail().getAddress(),
      password: await user.getPassword().getHash(),
      created: user.getCreated()
    }

    return await this.database.query(sql, values);
  }

  public async updateActivated(user: UserEntity): Promise<object>
  {
    let sql = " \
      UPDATE `account_user` \
      SET `account_user`.`activated` = :activated \
      WHERE `account_user`.`id` = :id \
    ";

    let values = {
      id: user.getId().getBuffer(),
      activated: user.getActivated()
    }

    return await this.database.query(sql, values);
  }
}
