import { Email } from '@dupkey/mail';
import Password from '@dupkey/password';
import { Payload, PayloadInterface, PayloadStatus } from '@dupkey/payload';
import Uuid from '@dupkey/uuid';
import UserEntity from './UserEntity';
import UserMessage from './UserMessage';
import UserRepository from './UserRepository';
import UserValidator from './UserValidator';

export default class UserService
{
  private userMessage: UserMessage;
  private userRepository: UserRepository;
  private userValidator: UserValidator;

  constructor(userMessage: UserMessage, userRepository: UserRepository, userValidator: UserValidator)
  {
    this.userMessage = userMessage;
    this.userRepository = userRepository;
    this.userValidator = userValidator;
  }

  public async create(name: string, email: string, password: string): Promise<PayloadInterface>
  {
    let payload = (new Payload()).setInput({ name, email, password });

    if (this.userValidator.forCreate(name, email, password) === false) {
      return payload.setStatus(PayloadStatus.NOT_VALID)
        .setErrors(this.userValidator.getErrors());
    }

    let user = await this.userRepository.fetchOneByEmail(email);
    if (user !== null) {
      if (user.getBanned() === null) {
        await this.userMessage.forExists(user);
      }

      // Do not divulge that the username exists in our system
      return payload.setStatus(PayloadStatus.ACCEPTED)
        .setOutput(new UserEntity(
          Uuid.v4(),
          name,
          new Email(email, name),
          new Password(password),
          new Date()
        ));
    }

    user = new UserEntity(
      Uuid.v4(),
      name,
      new Email(email, name),
      new Password(password),
      new Date()
    );

    await this.userRepository.save(user);
    await this.userMessage.forCreate(user);

    return payload.setStatus(PayloadStatus.CREATED).setOutput(user);
  }

  public async fetch(id: string): Promise<PayloadInterface>
  {
    let payload = (new Payload()).setInput({ id });

    if (this.userValidator.forFetch(id) === false) {
      return payload.setStatus(PayloadStatus.NOT_VALID)
        .setErrors(this.userValidator.getErrors());
    }

    let user = await this.userRepository.fetchOneById(Uuid.fromString(id));
    if (user === null ||
        user.getActivated() === null ||
        user.getBanned() !== null) {
          return payload.setStatus(PayloadStatus.NOT_FOUND);
    }
    
    return payload.setStatus(PayloadStatus.FOUND).setOutput(user);
  }

  public async updateActivated(id: string): Promise<PayloadInterface>
  {
    let payload = (new Payload()).setInput({ id });

    if (this.userValidator.forUpdateActivated(id) === false) {
      return payload.setStatus(PayloadStatus.NOT_VALID)
        .setErrors(this.userValidator.getErrors());
    }

    let user = await this.userRepository.fetchOneById(Uuid.fromString(id));
    if (user === null ||
        user.getActivated() !== null ||
        user.getBanned() !== null) {
          return payload.setStatus(PayloadStatus.NOT_UPDATED);
    }

    await this.userRepository.updateActivated(user.setActivated(new Date()));
    
    return payload.setStatus(PayloadStatus.UPDATED).setOutput(user);

  }
}