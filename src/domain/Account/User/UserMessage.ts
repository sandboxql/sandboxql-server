import { Mailer, Message, MessageSettings, ResponseInterface } from '@dupkey/mail';
import UserEntity from './UserEntity';

export default class UserMessage
{
  protected mailer: Mailer;
  protected message: MessageSettings;

  constructor(mailer: Mailer, message: MessageSettings)
  {
    this.mailer = mailer;
    this.message = message;
  }

  public async forCreate(user: UserEntity): Promise<ResponseInterface>
  {
    let message = new Message(
      user.getEmail(),
      this.message.forCreate.template,
      'User Registration',
      { id: user.getId().toString(), email: user.getEmail().getAddress() }
    );

    return await this.mailer.send(message);
  }

  public async forExists(user: UserEntity): Promise<ResponseInterface>
  {
    let message = new Message(
      user.getEmail(),
      this.message.forExists.template,
      'Account Access'
    );

    return await this.mailer.send(message);
  }
}