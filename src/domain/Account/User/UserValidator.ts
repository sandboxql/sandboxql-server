import { JoiValidator } from '@dupkey/validator';
import * as Joi from 'joi';

export default class UserValidator extends JoiValidator
{
  public forCreate(name: string, email: string, password: string): boolean
  {
    let input = {
      name,
      email,
      password
    }

    let rules = {
      name: Joi.string().min(3).max(45).required(),
      email: Joi.string().email({ minDomainAtoms: 2 }).required(),
      password: Joi.string().min(8).required(),
    };

    return this.validate(input, rules);
  }

  public forFetch(id: string)
  {
    let input = { id }

    let rules = {
      id: Joi.string().guid({ version: [ 'uuidv4' ] }).required()
    }

    return this.validate(input, rules);
  }

  public forUpdateActivated(id: string)
  {
    let input = { id }

    let rules = {
      id: Joi.string().guid({ version: [ 'uuidv4' ] }).required()
    }

    return this.validate(input, rules);
  }
}