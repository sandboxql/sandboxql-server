import { JoiValidator } from '@dupkey/validator';
import * as Joi from 'joi';

export default class AuthTokenValidator extends JoiValidator
{
  public forUserSignin(email: string, password: string): boolean
  {
    let input = {
      email,
      password
    }

    let rules = {
      email: Joi.string().email({ minDomainAtoms: 2 }).required(),
      password: Joi.string().min(8).required()
    };

    return this.validate(input, rules);
  }
}