import { FilterRootFields, mergeSchemas, transformSchema } from 'graphql-tools';
import authTokenSchema from './graphql/authTokenSchema';
import groupSchema from './graphql/groupSchema';
import memberSchema from './graphql/memberSchema';
import scalarTypeDate from './graphql/scalarType/scalarTypeDate';
import scalarTypeUUID from './graphql/scalarType/scalarTypeUUID';
import userSchema from './graphql/userSchema';

const overrideTypeDefs = `
  type AuthToken {
    id: UUID!
    user: User
    accessToken: String!
    refreshToken: String
    created: Date
  }

  type Group {
    id: UUID!
    parentId: UUID
    name: String!
    path: String
    description: String
    visibility: GroupVisibility!
    members: [Member]
    schema: String!
    created: Date!
    updated: Date
  }

  type Member {
    id: UUID!
    user: User
    scope: String!
    kind: String!
    created: Date!
  }
`;

const transformedUserSchema = transformSchema(userSchema, [
  new FilterRootFields((operation, fieldName, field) => {
    if (fieldName === 'userById') {
      return false;
    }
    return true;
  })
]);

const transformedMemberSchema = transformSchema(memberSchema, [
  new FilterRootFields((operation, fieldName, field) => {
    if (fieldName === 'membersByItemId') {
      return false;
    }
    return true;
  })
]);

export default mergeSchemas({
  schemas: [
    scalarTypeDate,
    scalarTypeUUID,
    authTokenSchema,
    groupSchema,
    transformedMemberSchema,
    transformedUserSchema,  
    overrideTypeDefs
  ],
  resolvers: {
    AuthToken: {
      user: {
        fragment: 'fragment AuthTokenFragment on AuthToken { userId }',
        resolve(authToken: any, {}, context: any, info: any) {
          return info.mergeInfo.delegateToSchema({
            schema: userSchema,
            operation: 'query',
            fieldName: 'userById',
            args: {
              id: String(authToken.userId)
            },
            context,
            info
          });
        }
      }
    },

    Group: {
      members: {
        fragment: 'fragment GroupFragment on Group { id }',
        resolve(group: any, {}, context: any, info: any) {
          return info.mergeInfo.delegateToSchema({
            schema: memberSchema,
            operation: 'query',
            fieldName: 'membersByItemId',
            args: {
              itemId: String(group.id)
            },
            context,
            info
          });
        }
      }
    },

    Member: {
      user: {
        fragment: 'fragment MemberFragment on Member { userId }',
        resolve(member: any, {}, context: any, info: any) {
          return info.mergeInfo.delegateToSchema({
            schema: userSchema,
            operation: 'query',
            fieldName: 'userById',
            args: {
              id: String(member.userId)
            },
            context,
            info
          });
        }
      }
    },
  }
});