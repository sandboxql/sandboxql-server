import { ApolloServer } from 'apollo-server-lambda';
import * as middy from 'middy';
import schema from './schema';
import { mariadbMiddleware } from '@dupkey/mariadb-lambda';
import { Container, containerMiddleware } from '@dupkey/container';
import dependencies from './dependencies';

const server = new ApolloServer({
  schema,
  context: ({ event, context }) => ({
    headers: event.headers,
    functionName: context.functionName,
    event,
    context,
    playground: {
      endpoint: process.env.PLAYGROUND_ENDPOINT
    }
  })
});

const serverHandler = server.createHandler({
  cors: {
    origin: String(process.env.CORS).split(' '),
    credentials: true,
  }
});

export const handler = middy(serverHandler)
  .use(mariadbMiddleware(
    String(process.env.MARIADB_HOST),
    String(process.env.MARIADB_USER),
    String(process.env.MARIADB_PASSWORD),
    String(process.env.MARIADB_DATABASE),
    { namedPlaceholders: true }
  ))
  .use(containerMiddleware(new Container(dependencies), new Map([
    ['connection', { singleton: true }]
  ])));
